<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Students;
use Illuminate\Support\Facades\Storage;

class ExportController extends Controller
{
    public function __construct()
    {

    }

    public function welcome()
    {
        return view('hello');
    }

    /**
     * View all students found in the database
     */
    public function viewStudents()
    {
        $students = Students::with('course')->get();
        return view('view_students', compact(['students']));
    }

    /**
     * Exports all student data to a CSV file
     */
    public function exportStudentsToCSV(Request $request)
    {
        $to_csv = []; // container
        $filename = "export-" . \Carbon\Carbon::now()->timestamp . ".csv"; // filename of csv

        $ids = $request->get('studentId');

        // redirect back if no selected
        if (count($ids) < 1) {
            return \Redirect::back();
        }

        // run and get data by ID
        foreach($ids as $id) {
            $student = Students::with('course')->find($id);

            array_push($to_csv, $student->toArray()); // push a data into the container
        }


        //create file using Filesystem
        Storage::disk('csv')->put($filename, "");

        // handler or cursor
        $fp = fopen('csv/' . $filename, 'w');

        // populate data into the csv file
        foreach ($to_csv as $file) {
            $result = []; // container

            // for multidimension array
            array_walk_recursive($file, function($item) use (&$result) {
                $result[] = $item;
            });

            // create a csv and put the data
            fputcsv($fp, $result);
        }

        // closing handler or cursor
        fclose($fp);

        // get the csv
        $file= public_path(). "/csv/" . $filename;

        // set headers
        $headers = array(
            'Content-Type: text/csv',
        );

        // download the csv
        return \Response::download($file, $filename, $headers);

    }

    /**
     * Exports the total amount of students that are taking each course to a CSV file
     */
    public function exporttCourseAttendenceToCSV()
    {

    }
}
